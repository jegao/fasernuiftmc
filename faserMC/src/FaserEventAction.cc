// adapted from Geant4 example

#include "FaserEventAction.hh"
#include "FaserRunAction.hh"

#include "G4Event.hh"
#include "FaserEvent.hh"
#include "FaserEventInformation.hh"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "FaserDigi.hh"
#include "FaserTrackerEvent.hh"
#include "FaserTrackerSpacePoint.hh"
#include "FaserTrackerCluster.hh"
#include "FaserTrackerDigit.hh"
#include "FaserTrackerTruthHit.hh"
#include "FaserTrackerTruthParticle.hh"
#include "RootEventIO.hh"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//JS
#include "Analysis.hh"
//JS
#include "G4RunManager.hh"

#include "FaserSensorHit.hh"
#include "FaserCaloHit.hh"

#include "G4DigiManager.hh"
#include "FaserDigitizer.hh"
#include "FaserCaloDigitizer.hh"

#include "FaserDigi.hh"
#include "FaserCaloDigi.hh"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "CalorHit.hh"
#include "CalorimeterSD.hh"
#include "G4SDManager.hh"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "TVector3.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FaserEventAction::FaserEventAction(FaserRunAction* runAction)
  : G4UserEventAction()
  , fRunAction(runAction)
  , fCalorHCID(-1)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  , fFaserEvent(nullptr)
  , fFaserTrackerEvent(nullptr)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

{
  FaserDigitizer* fd = new FaserDigitizer("FaserTrackerDigitizer", "FaserTrackerHitsCollection", "FaserTrackerDigiCollection");
  G4DigiManager::GetDMpointer()->AddNewModule(fd);
  fd = new FaserDigitizer("FaserSamplerDigitizer", "FaserSamplerHitsCollection", "FaserSamplerDigiCollection");
  G4DigiManager::GetDMpointer()->AddNewModule(fd);

  FaserCaloDigitizer* fcd = new FaserCaloDigitizer("FaserCaloDigitizer", "FaserCaloHitsCollection", "FaserCaloDigiCollection");
  G4DigiManager::GetDMpointer()->AddNewModule(fcd);
} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FaserEventAction::~FaserEventAction()
{
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  if (fFaserEvent != nullptr) delete fFaserEvent;
  fFaserEvent = nullptr;
  if (fFaserTrackerEvent != nullptr) delete fFaserTrackerEvent;
  fFaserTrackerEvent = nullptr;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CalorHitsCollection* FaserEventAction::GetCalorHitsCollection(G4int hcID, const G4Event* event) const
{
  auto hitsCollection =
    static_cast<CalorHitsCollection*>(event->GetHCofThisEvent()->GetHC(hcID));
  
  if ( ! hitsCollection ) {
    G4ExceptionDescription msg;
    msg << "Cannot access hitsCollection ID " << hcID; 
    G4Exception("EventAction::GetCalorHitsCollection()",
      "MyCode0003", FatalException, msg);
  }
  return hitsCollection;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void FaserEventAction::BeginOfEventAction(const G4Event* event)
{
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  if (fFaserEvent != nullptr) delete fFaserEvent;
//  fFaserEvent = new FaserEvent(event->GetEventID());
  if (fFaserTrackerEvent != nullptr) delete fFaserTrackerEvent;
  fFaserTrackerEvent = new FaserTrackerEvent(event->GetEventID());

  auto sdManager = G4SDManager::GetSDMpointer();
  fCalorHCID   = sdManager->GetCollectionID("EmulsionHitsCollection");
  cham.clear();
  idz.clear();
  idzsub.clear();
  pdgid.clear();
  id.clear();
  idParent.clear();
  charge.clear();
  x.clear();
  y.clear();
  z.clear();
  px.clear();
  py.clear();
  pz.clear();
  e1.clear();
  e2.clear();
  len.clear();
  edep.clear();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FaserEventAction::EndOfEventAction(const G4Event* g4Evt)
{ 
  FaserEvent* faserEvent = new FaserEvent();

  G4DigiManager* digiMan = G4DigiManager::GetDMpointer();

  FaserDigitizer* trackerDigiModule = (FaserDigitizer*) digiMan->FindDigitizerModule("FaserTrackerDigitizer");
  trackerDigiModule->Digitize();

  FaserDigitizer* samplerDigiModule = (FaserDigitizer*) digiMan->FindDigitizerModule("FaserSamplerDigitizer");
  samplerDigiModule->Digitize();

  FaserCaloDigitizer* caloDigiModule = (FaserCaloDigitizer*) digiMan->FindDigitizerModule("FaserCaloDigitizer");
  caloDigiModule->Digitize();

  G4int trackerDigiID = digiMan->GetDigiCollectionID("FaserTrackerDigiCollection");
  FaserDigiCollection* tdc = (FaserDigiCollection*)
    digiMan->GetDigiCollection(trackerDigiID);

  G4int samplerDigiID = digiMan->GetDigiCollectionID("FaserSamplerDigiCollection");
  FaserDigiCollection* sdc = (FaserDigiCollection*)
    digiMan->GetDigiCollection(samplerDigiID);

  G4int caloDigiID = digiMan->GetDigiCollectionID("FaserCaloDigiCollection");
  FaserCaloDigiCollection* cdc = (FaserCaloDigiCollection*)
    digiMan->GetDigiCollection(caloDigiID);

  G4int trackerID = digiMan->GetHitsCollectionID("FaserTrackerHitsCollection");
  FaserSensorHitsCollection* hc = (FaserSensorHitsCollection*)
    digiMan->GetHitsCollection(trackerID);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  G4int emulsionID = digiMan->GetHitsCollectionID("EmulsionHitsCollection");
  CalorHitsCollection* ec = (CalorHitsCollection*)
    digiMan->GetHitsCollection(0); // For some reason this hit collection has ID 0
  std::cout << "Emulsion hits collection has size " << ec->GetSize() << std::endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  G4int samplerID = digiMan->GetHitsCollectionID("FaserSamplerHitsCollection");
  FaserSensorHitsCollection* sc = (FaserSensorHitsCollection*)
    digiMan->GetHitsCollection(samplerID);

  G4int caloID = digiMan->GetHitsCollectionID("FaserCaloHitsCollection");
  FaserCaloHitsCollection* cc = (FaserCaloHitsCollection*)
    digiMan->GetHitsCollection(caloID);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  auto calorHC = GetCalorHitsCollection(fCalorHCID, g4Evt);

  for (unsigned long i = 0; i < ec->GetSize(); ++i) {
    auto hit = static_cast<CalorHit*>(ec->GetHit(i));
    if(hit && hit->GetEnergyDepo()>1*keV) {
      cham.push_back(hit->GetChamber());
      idz.push_back(hit->GetIDZ());
//    std::cout << "Pushing back idz with value " << hit->GetIDZ() << std::endl;
      idzsub.push_back(hit->GetIDZsub());
      pdgid.push_back(hit->GetParticleID());
      id.push_back(hit->GetTrackID());
      idParent.push_back(hit->GetParentID());
      charge.push_back(hit->GetCharge());
      x.push_back(hit->GetPosition().x()/mm);
      y.push_back(hit->GetPosition().y()/mm);
      z.push_back(hit->GetPosition().z()/mm);
      px.push_back(hit->GetMomentum().x()/MeV);
      py.push_back(hit->GetMomentum().y()/MeV);
      pz.push_back(hit->GetMomentum().z()/MeV);
      e1.push_back(hit->GetEnergyPreS()/MeV);
      e2.push_back(hit->GetEnergyPost()/MeV);
      len.push_back(hit->GetTrackLength()/mm);
      edep.push_back(hit->GetEnergyDepo()/MeV);
    } } 
  auto analysisManager = G4AnalysisManager::Instance();

  analysisManager->FillNtupleDColumn(0, e_beam*1000); // MeV
  analysisManager->FillNtupleIColumn(1, id_beam);
  analysisManager->FillNtupleDColumn(2, x_beam*10); // mm
  analysisManager->FillNtupleDColumn(3, y_beam*10); // mm

  analysisManager->FillNtupleIColumn(4, pdgnu_nuEvt);
  analysisManager->FillNtupleIColumn(5, pdglep_nuEvt);
  analysisManager->FillNtupleDColumn(6, Enu_nuEvt*1000); // MeV
  analysisManager->FillNtupleDColumn(7, Plep_nuEvt*1000); // MeV
  analysisManager->FillNtupleIColumn(8, cc_nuEvt);
  analysisManager->FillNtupleDColumn(9, x_nuEvt*10); // mm
  analysisManager->FillNtupleDColumn(10, y_nuEvt*10); // mm
  analysisManager->FillNtupleDColumn(11, z_nuEvt*10); // mm
  
  // fill ntuple
  analysisManager->AddNtupleRow();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  faserEvent->SetParticles(g4Evt->GetTrajectoryContainer());
  faserEvent->SetTrackerHits(hc);
  faserEvent->SetSamplerHits(sc);
  faserEvent->SetCaloHits(cc);                                                                                                  
  faserEvent->SetTrackerDigis(tdc);
  faserEvent->SetSamplerDigis(sdc);
  faserEvent->SetCaloDigis(cdc);
  faserEvent->SetClusters();
  faserEvent->SetSpacePoints();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  for (FaserSensorHit * hit : faserEvent->TrackerHits()) {
    fFaserTrackerEvent->truthHits.push_back(new FaserTrackerTruthHit {
      hit->Track(),
      hit->Plane(),
      hit->Module(),
      hit->Sensor(),
      hit->Row(),
      hit->Strip(),
      hit->Edep(),
      {hit->GlobalPos().x(), hit->GlobalPos().y(), hit->GlobalPos().z()}
    });
  }
  for (FaserDigi * digi : faserEvent->TrackerDigis()) {
    G4ThreeVector globalPos = digi->Transform().NetTranslation();
    auto * trDigit = new FaserTrackerDigit {
      digi->Plane(),
      digi->Module(),
      digi->Sensor(),
      digi->Row(),
      digi->Strip(),
      digi->Charge(),
      {globalPos.x(), globalPos.y(), globalPos.z()}
    };
//std::cout << "Looping over tracker digits..." << std::endl;
    for (uint i = 0; i < fFaserTrackerEvent->truthHits.size(); ++i) {
      FaserTrackerTruthHit * hit = fFaserTrackerEvent->truthHits[i];
      if (hit->plane  != trDigit->plane ) continue;
      if (hit->module != trDigit->module) continue;
      if (hit->sensor != trDigit->sensor) continue;
      if (hit->row    != trDigit->row   ) continue;
      if (hit->strip  != trDigit->strip ) continue;
      trDigit->truthHitIndices.push_back(i);
//std::cout << "Pushing back truthHits to trDigit..." << std::endl;
    }
    fFaserTrackerEvent->digits.push_back(trDigit);
//  std::cout << "Pushing back trDigit to fFaserTrackerEvent..." << std::endl;
  }

  for (FaserCluster * cl : faserEvent->Clusters()) {
    auto * trCluster = new FaserTrackerCluster {
      cl->Plane(),
      cl->Module(),
      cl->Sensor(),
      cl->Row(),
      cl->WeightedStrip(),
      cl->Charge(),
      {cl->GlobalPosition().x(), cl->GlobalPosition().y(), cl->GlobalPosition().z()}
    };
//  std::cout << "Looping over clusters..." << std::endl;
    for (uint i = 0; i < faserEvent->TrackerDigis().size(); ++i) {
      FaserDigi * eventDigi = faserEvent->TrackerDigis().at(i);
      if (std::find(cl->Digis().begin(), cl->Digis().end(), eventDigi) != cl->Digis().end()) {
        trCluster->digitIndices.push_back(i);
// std::cout << "Pushing back digits into trCluster..." << std::endl;
      }
    }
    for (uint i = 0; i < trCluster->digitIndices.size(); ++i) {
      FaserTrackerDigit * trDigit = fFaserTrackerEvent->digits[i];
      for (uint j : trDigit->truthHitIndices) {
        if (std::find(trCluster->truthHitIndices.begin(), trCluster->truthHitIndices.end(), j) != trCluster->truthHitIndices.end()) {
            trCluster->truthHitIndices.push_back(j);
        }
      }
    }
    fFaserTrackerEvent->analogClusters.push_back(trCluster);
//  std::cout << "Pushing back trCluster into fFaserTrackerEvent..." << std::endl;
  }

  for (FaserSpacePoint * sp : faserEvent->SpacePoints()) {
    auto * trSP = new FaserTrackerSpacePoint {
      sp->Plane(),
      sp->Module(),
      sp->Sensor(),
      sp->Row(),
      {sp->GlobalPosition().x(), sp->GlobalPosition().y(), sp->GlobalPosition().z()}
     };
//   std::cout << "Looping over SPs..." << std::endl;
    for (uint i = 0; i < faserEvent->Clusters().size(); ++i) {
      FaserCluster * eventCl = faserEvent->Clusters().at(i);
      if (std::find(sp->Clusters().begin(), sp->Clusters().end(), eventCl) != sp->Clusters().end()) {
        trSP->analogClusterIndices.push_back(i);
// std::cout << "Pushing back cluster indices into trSP..." << std::endl;
      }
    }
    for (uint i = 0; i < trSP->analogClusterIndices.size(); ++i) {
      FaserTrackerCluster * trCluster = fFaserTrackerEvent->analogClusters[i];
      for (uint j : trCluster->truthHitIndices) {
        if (std::find(trSP->truthHitIndices.begin(), trSP->truthHitIndices.end(), j) != trSP->truthHitIndices.end()) {
          trSP->truthHitIndices.push_back(j);
        }
      }
    }
    fFaserTrackerEvent->spacePoints.push_back(trSP);
//std::cout << "Pushing back trSP into fFaserTrackerEvent..." << std::endl;
  }
  for (FaserTruthParticle * tp : faserEvent->Particles()) {
 const G4ThreeVector & vertex = tp->Vertex();
 const G4ThreeVector & momentum = tp->Momentum();
    fFaserTrackerEvent->truthParticles.push_back(new FaserTrackerTruthParticle {
      tp->TrackID(),
      tp->ParentID(),
      tp->PdgCode(),
      TVector3{vertex.x(), vertex.y(), vertex.z()},
      TLorentzVector{momentum.x(), momentum.y(), momentum.z(), tp->Energy()},
    });
  }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // Attach our event to the G4Event so it can be retrieved elsewhere (for I/O and visualization)
  // G4 will manage the memory after this call
  G4EventManager::GetEventManager()->SetUserInformation(new FaserEventInformation(faserEvent));
  g4Evt->GetUserInformation()->Print();

  RootEventIO* rootEventIO = RootEventIO::GetInstance();
  rootEventIO->Write(faserEvent);
  rootEventIO->Write(fFaserTrackerEvent);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
