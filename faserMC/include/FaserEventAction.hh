// adapted from Geant4 example
#ifndef FASER_EVENTACTION_HH
#define FASER_EVENTACTION_HH 1

#include "G4UserEventAction.hh"
#include "globals.hh"

#include "CalorHit.hh"
#include "CalorimeterSD.hh"
class FaserRunAction;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class FaserEvent;
class FaserTrackerEvent;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/// Event action class
///

class FaserEventAction : public G4UserEventAction
{
  public:
    FaserEventAction(FaserRunAction* runAction);
    virtual ~FaserEventAction();

    virtual void BeginOfEventAction(const G4Event* event);
    virtual void EndOfEventAction(const G4Event* event);

  private:
    FaserRunAction*     fRunAction;
    FaserEvent*         fFaserEvent;
    FaserTrackerEvent*  fFaserTrackerEvent;
    CalorHitsCollection* GetCalorHitsCollection(G4int hcID, const G4Event* event) const;
    G4int  fCalorHCID;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

